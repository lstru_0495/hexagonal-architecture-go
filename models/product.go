package models

import (
	"gopkg.in/mgo.v2/bson"
)

type Product struct {
	ID          bson.ObjectId `bson:"_id" json: "id"`
	Name        string        `bson:"name" json: "name"`
	Description string        `bson:"description" json: "description"`
	Price       string        `bson:"orice" json: "price"`
}

func (p Product) String() string {
	return "Name: " + p.Name + "\nDescription: " + p.Description + "\nPrice: " + p.Price
}
