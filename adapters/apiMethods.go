package adapters

import (
	"encoding/json"
	. "food/models"
	. "food/ports"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
)

type APIAdapter struct {
	//Variable usada para acceso a la base de datos
	//Se usa tipo Repository, para que luego sea asignada
	// a un tipo especifico que la implemente
	Dao Repository
}

// POST
func (api APIAdapter) CreateUserHandler(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var user User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		log.Fatal(err)
		return
	}
	user.ID = bson.NewObjectId()
	err = api.Dao.InsertUser(user)
	if err != nil {
		log.Fatal(err)
		return
	}
	json.NewEncoder(w).Encode(user)
}

//GET
func (api APIAdapter) GetUserHandler(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	params := mux.Vars(r)
	id := params["id"]

	err, user := api.Dao.FindUserById(id)
	//println(err)
	if err != nil {
		log.Fatal(err)
		return
	}

	json.NewEncoder(w).Encode(user)
}

func (api APIAdapter) GetAllUsersHandler(w http.ResponseWriter, r *http.Request) {
	_, users := api.Dao.FindAllUsers()
	/*for _, user := range users {
		fmt.Println(user)
	}*/
	json.NewEncoder(w).Encode(users)
}

//DELETE
func (api APIAdapter) DeleteUserHandler(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	params := mux.Vars(r)
	id := params["id"]

	err := api.Dao.DeleteUserById(id)
	if err != nil {
		log.Fatal(err)
		return
	}
}

//PUT
func (api APIAdapter) UpdateUserHandler(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	params := mux.Vars(r)
	id := params["id"]

	var user User
	user.ID = bson.ObjectIdHex(id)
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		log.Fatal(err)
		return
	}
	err = api.Dao.UpdateUserById(id, user)
	if err != nil {
		log.Fatal(err)
		return
	}

}
