package adapters

import (
	//"fmt"
	. "food/models"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
)

//Soporte para MongoDB
var (
	db *mgo.Database
)

type MongoDbRepo struct {
	URL      string
	Database string
}

const (
	COLLECTION = "users"
)

func (m MongoDbRepo) Connect() {
	session, err := mgo.Dial(m.URL)
	if err != nil {
		log.Fatal(err)
	}
	db = session.DB(m.Database)
}

/*func (m *MongoDbRepo) CheckConnection() bool {
	if db == nil {
		m.Connect()
	} else {
		return true
	}
	return false
}*/

func (m MongoDbRepo) InsertUser(user User) error {
	err := db.C(COLLECTION).Insert(&user)
	return err
}

func (m MongoDbRepo) FindUserById(id string) (error, User) {
	user := User{}
	//err := db.C(COLLECTION).FindId(bson.ObjectIdHex(id)).One(&user)
	err := db.C(COLLECTION).Find(bson.M{"id": bson.ObjectIdHex(id)}).One(&user)
	return err, user
}

func (m MongoDbRepo) FindByUsername(username string) (error, User) {
	user := User{}
	err := db.C(COLLECTION).Find(bson.M{"username": username}).One(&user)
	return err, user
}

func (m MongoDbRepo) FindAllUsers() (error, []User) {
	var users []User
	userFoo := &User{}

	iter := db.C(COLLECTION).Find(nil).Iter()
	for iter.Next(&userFoo) {
		users = append(users, *userFoo)
	}
	return nil, users
}

func (m MongoDbRepo) DeleteUserById(id string) error {
	err := db.C(COLLECTION).Remove(bson.M{"id": bson.ObjectIdHex(id)})
	return err
}

func (m MongoDbRepo) UpdateUserById(id string, user User) error {
	err := db.C(COLLECTION).Update(bson.M{"id": bson.ObjectIdHex(id)}, &user)
	return err
}
