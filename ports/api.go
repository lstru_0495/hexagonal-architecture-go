package ports

import (
	"net/http"
)

type API interface {
	CreateUserHandler(http.ResponseWriter, *http.Request)
	GetUserHandler(http.ResponseWriter, *http.Request)
	GetAllUsersHandler(http.ResponseWriter, *http.Request)
	DeleteUserHandler(http.ResponseWriter, *http.Request)
	UpdateUserHandler(http.ResponseWriter, *http.Request)
}
