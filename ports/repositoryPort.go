package ports

import (
	. "food/models"
)

//Se usar como port para acceder a la base de datos
type Repository interface {
	//Funciones que debe implementar un adapter para ser incoporparada a la arquitectura
	Connect()
	InsertUser(user User) error
	FindUserById(id string) (error, User)
	FindAllUsers() (error, []User)
	FindByUsername(username string) (error, User)
	DeleteUserById(id string) error
	UpdateUserById(id string, user User) error
}
