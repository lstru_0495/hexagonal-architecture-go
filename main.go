package main

import (
	. "food/adapters"
	. "food/ports"
	"github.com/gorilla/mux"

	"log"
	"net/http"
)

var dao Repository
var api APIAdapter

func init() {
	url := "127.0.0.1:27017"
	db := "test"
	dao = MongoDbRepo{url, db}
	dao.Connect()

	api.Dao = dao

}

func main() {
	//	client := Client{User{"Name", "SName", "11313-2313", "a@d.com", "user1", "pass1"}, "123456"}
	r := mux.NewRouter()

	r.HandleFunc("/users", api.GetAllUsersHandler).Methods("GET")
	r.HandleFunc("/users/{id}", api.GetUserHandler).Methods("GET")
	r.HandleFunc("/users", api.CreateUserHandler).Methods("POST")
	r.HandleFunc("/users/{id}", api.DeleteUserHandler).Methods("DELETE")
	r.HandleFunc("/users/{id}", api.UpdateUserHandler).Methods("PUT")

	//r.HandleFunc("/login", login).Methods("GET")
	if err := http.ListenAndServe(":9090", r); err != nil {
		log.Fatal(err)
	}
}

/*

{
	"name": "Name3",
	"surname":"Surname3",
	"phone":"91245122",
	"email":"user3@users.com",
	"username":"user3",
	"password":"423446"
}

curl http://localhost:9090/users/5b0a25765ed29377ce7ff364


*/
